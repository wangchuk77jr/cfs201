import React from "react";
import GoalList from "./Components/GoalList";
import Header from "./Components/Header";

function App() {
  return (
    <div style={{marginTop:'10px',display:'flex',justifyContent:'center',alignItems:'center'}}>
    <div style={{border:'2px solid #000',width:'70%'}}>
      <Header/>
      <GoalList/>
    </div>
    </div>
  );
}

export default App;
