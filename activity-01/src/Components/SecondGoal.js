import React from 'react'

const SecondGoal = () => {
  return (
    <div>
    <ul>
        <li>
            <h4>Teach React in a highly-understandable way</h4>
        </li>
        <p>I want ensure that you get the most out of this book and you learn all about React!</p>
    </ul>
   
</div>
  )
}

export default SecondGoal;