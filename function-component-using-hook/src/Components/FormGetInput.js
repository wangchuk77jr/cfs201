import React,{useState} from 'react'

function FormGetInput() {
    const [message,setMessage] = useState('');
    const [update,setUpdated] = useState(message);

    const OnChangeHandler = (e) => {
        setMessage(e.target.value);
    };
    const handleClick = () =>{
        setUpdated(message);
    }
  return (
    <div style={{margin:"20px"}}>
        <input type='text' name='message' id='message' onChange={OnChangeHandler} style={{padding:'10px',borderRadius:'10px',}} />
        <h1>Message: {message}</h1>
        <h1>Updated: {update} </h1>
        <button onClick={handleClick} style={{paddingInline:'30px',paddingTop:'10px',paddingBottom:'10px', fontWeight:'900'}}>Update</button>
    </div>
  )
};

export default FormGetInput;