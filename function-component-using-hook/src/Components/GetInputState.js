import React, { Component } from 'react'

export class GetInputState extends Component {
    constructor(props){
        super(props);
        this.state = {inputText:''};
    }
  render() {
    return (
      <div>
        <input value={this.state.inputText}
        onChange ={(e) =>{
            this.setState({inputText:e.target.value})
        }}
        />
        <button onClick={()=>{alert(this.state.inputText)}}>Click Me</button>
      </div>
    )
  }
}

export default GetInputState