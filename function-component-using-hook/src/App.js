import './App.css';
import FormGetInput from './Components/FormGetInput';
import GetInputState from './Components/GetInputState';

function App() {
  return (
    <div className="App">
      <h1 style={{color:'red'}}>Functional Component</h1>
      <FormGetInput/>
      <h1 style={{color:'red'}}>Class Component</h1>
      <GetInputState/>
    </div>
  );
}

export default App;
