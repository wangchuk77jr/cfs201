import React from "react";
import './tutor.css'
const MyTuturs = () =>{
    return(
        <div className="container mb-5">
            <div className="card py-3">
                <div className="text-center mb-3">
                    <img className="tutor-img" src="https://www.gcit.edu.bt/wp-content/uploads/sites/3/2021/04/team_jigme_wangmo.jpg" style={{borderRadius:'100px',width:'50%'}}/>
                </div>
                <div  className="px-4 py -3">
                <p className="tutor-dec">Name: <span>Jigme Wangmo</span></p>
                <p className="tutor-dec">Module: <span>CFS201</span></p>
                </div>

            </div>
            <div className="card py-3">
                <div className="text-center mb-3">
                    <img className="tutor-img" src="https://www.gcit.edu.bt/wp-content/uploads/sites/3/2021/04/team_karma_dorji.jpg" style={{borderRadius:'100px',width:'50%'}}/>
                </div>
                <div  className="px-4 py -3">
                <p className="tutor-dec">Name: <span>Karma Dorji</span></p>
                <p className="tutor-dec">Module: <span>ELE101</span></p>
                </div>

            </div>
            <div className="card py-3">
                <div className="text-center mb-3">
                    <img className="tutor-img" src="https://www.gcit.edu.bt/wp-content/uploads/sites/3/2021/11/team_ong-1.jpg" style={{borderRadius:'100px',width:'50%'}}/>
                </div>
                <div  className="px-4 py -3">
                <p className="tutor-dec">Name: <span>Mr. ONG</span></p>
                <p className="tutor-dec">Module: <span>DCM102</span></p>
                </div>

            </div>
            <div className="card py-3">
                <div className="text-center mb-3">
                    <img className="tutor-img" src="https://www.gcit.edu.bt/wp-content/uploads/sites/3/2021/11/Staff-Photos-Rebecca-Tirwa_Color4.jpg" style={{borderRadius:'100px',width:'50%'}}/>
                </div>
                <div  className="px-4 py -3">
                <p className="tutor-dec">Name: <span>Rebbaca Terwa</span></p>
                <p className="tutor-dec">Module: <span>PRJ301</span></p>
                </div>

            </div>

     

        </div>
    );
}

export default MyTuturs;