import React,{Component} from "react";
import './course.css'
class MyCourse extends Component{
    render(){
        return(
            <div className="container">
                <div className="card">
                 <img src="https://toppng.com/uploads/preview/react-js-logo-11563036900lwsthywug1.png" alt="course-image" style={{height:'10rem',objectFit:'contain'}}/>
                 <div className="px-4 py -3">
                 <h4>CFS201</h4>
                 <p>This module aims to allow the students to advance their knowledge in front-end web development by using modern front-end frameworks.</p>
                 <a href="https://vle.gcit.edu.bt/course/view.php?id=130" className="btn">View More In Detials</a>
                 </div>
                </div>

                <div className="card">
                 <img src="https://w7.pngwing.com/pngs/525/108/png-transparent-hacker-computer-cybersecurity-fraud-laptop-hacking-thumbnail.png" alt="course-image" style={{height:'10rem',objectFit:'contain'}}/>
                 <div className="px-4 py -3">
                 <h4>ELE101</h4>
                 <p>The proliferation of the cyber realm has brought about a plethora of qualitative changes in various spheres of our lives.</p>
                 <a href="https://vle.gcit.edu.bt/course/view.php?id=111" className="btn">View More In Detials</a>
                 </div>
                </div>

                <div className="card">
                 <img src="https://toppng.com/uploads/preview/adobe-illustrator-cc-logo-11549874996szo4jbtzut.png" alt="course-image" style={{height:'10rem',objectFit:'contain'}}/>
                 <div className="px-4 py -3 mb-4">
                 <h4>DCM102</h4>
                 <p>a plan or drawing produced to show the look and function or workings of a building, garment, or other object before it is made</p>
                 <a href="https://vle.gcit.edu.bt/course/view.php?id=124" className="btn">View More In Detials</a>
                 </div>
                </div>

                <div className="card">
                 <img src="https://banner2.cleanpng.com/20180703/skw/kisspng-project-management-project-plan-project-manager-5b3c2e543409b8.0219046115306706762132.jpg" alt="course-image" style={{height:'10rem',objectFit:'contain',pattingTop:'2px',}}/>
                 <div className="px-4 py -3">
                 <h4>PRJ301</h4>
                 <p>Find Machine Learning Projects Ideas. Now with us! Search For Machine Learning Projects Ideas at Productopia.com. Attractive Results.</p>
                 <a href="https://vle.gcit.edu.bt/course/view.php?id=131" className="btn">View More In Detials</a>
                 </div>
                </div>
            </div>
        );
    }
}

export default MyCourse;