
import './App.css';
import React from 'react';
import MyCourse from './components/Course';
import MyTutors from './components/Tutor';
function App() {
  return (
    <div className='container'>
      <h1 style={{fontWeight:'900',textTransform:'uppercase'}}>My Courses</h1>
      <MyCourse/>
      <h1 style={{fontWeight:'900',textTransform:'uppercase',paddingTop:'20px'}}>My Tutors</h1>
      <MyTutors/>
    </div>
  );
}

export default App;
