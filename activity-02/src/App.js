import './App.css';
import NumberGuessing from './Components/GuessingGame';

function App() {
  return (
    <div className="App">
     <NumberGuessing/>
    </div>
  );
}

export default App;
