import React, { Component } from 'react';

class NumberGuessing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      score: 0,
      guess: '',
    };
  }

  checkNumber = () => {
    const randomNumber = Math.floor(Math.random() * 10) + 1;
    if (Number(this.state.guess) === randomNumber) {
      this.setState((prevState) => ({
        score: prevState.score + 1,
      }));
    }
  };

  handleGuessChange = (e) => {
    this.setState({
      guess: e.target.value,
    });
  };

  render() {
    const { score, guess } = this.state;
    return (
      <>
        What number (between 1 and 10) am I thinking of?{' '}
        <input
          value={guess}
          type="number"
          min="1"
          max="10"
          onChange={this.handleGuessChange}
        />
        <button onClick={this.checkNumber}>Guess!</button>
        <p>Your score: {score}</p>
      </>
    );
  }
}

export default NumberGuessing;
